window.onload = function () {

    //burger menu
    $('.modal-window .button').click(function () {
        $('.modal-overlay').hide()
		});
		
		$('.modal_trigger').click(function (e) {
			e.preventDefault();
			let thisHref = $(this).attr('href');
			$(thisHref).addClass('modal_show');
            $('.section-modals').addClass('modal_overlay');
	})
		//
		
		$('.modal__can').click(function(){
			$(this).parent().parent().removeClass('modal_show');
			$('.section-modals').removeClass('modal_overlay');
        });
        
        $('.modal_firstauth .button_y').click(function(){
            $('.modal#firstauth').removeClass('modal_show');
        });
        $('.modal_firstauth .button_n').click(function(){
            $('.modal#firstauth').removeClass('modal_show');
        });

        $('.modal_auth .modal_trigger').click(function(){
            $('.modal#auth').removeClass('modal_show');
        });

//input mask
    (function () {
        $("input[type='tel']").mask("+7999 - 999-99-99");
        //$('.field_table_cal input').mask('99 - 99 - 99');
        $(".modal_getgift input[placeholder='ДД/ММ/ГГ' ]").mask('99/99/99');
    })();


    //tabs
    (function () {
        $(".tab_wrapper .tab").removeClass("active");
        $(".tab_item").not(":first").hide();
        $(".tab_wrapper .tab").click(function () {
            $(".tab_wrapper .tab").removeClass("active").eq($(this).index()).addClass("active");
            $(".tab_item").hide().eq($(this).index()).fadeIn()
        }).eq(0).addClass("active");
    })();
    

    //up to top
    (function () {
        if ($('.up').length) {
            var scrollTrigger = 1000, // px
            backToTop = function () {
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > scrollTrigger) {
                        $('.up').addClass('up_showed');
                    } else {
                        $('.up').removeClass('up_showed');
                    }
                };
            backToTop();
            $(window).on('scroll', function () {
                backToTop();
            });
            $('.up').on('click', function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            });
        }
    })();

    $('.menu').click(function(e){
        //e.stopPropagation();
        $('.header_container').toggleClass('header_bg_anim');
        $('.header__inner').toggleClass('header_inner_anim');
        $('.nav').toggleClass('header_nav_anim')

    })

    $('.js-example-basic-single').select2();
    

    // $(".section-main.main_sale").mCustomScrollbar({
    //     theme:'rounded'
    // });
    
};


// (function () {
//     $.datepicker.setDefaults({
//         closeText: 'Закрыть',
//         prevText: '',
//         currentText: 'Сегодня',
//         monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
//             'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
//         monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
//             'Июл','Авг','Сен','Окт','Ноя','Дек'],
//         dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
//         dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
//         dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
//         weekHeader: 'Не',
//         dateFormat: 'dd.mm.yy',
//         firstDay: 1,
//         isRTL: false,
//         showMonthAfterYear: false,
//         yearSuffix: ''
//     });
//     $('.datepicker').datepicker();

// })();

// $('.partners-slider').slick({
//     slidesToShow: 4,
//     slidesToScroll: 4,
//     dots: true,
//     prevArrow: false,
//     nextArrow: false,
//     autoplay: true,
//     autoplaySpeed: 3000,
//     // vertical: true
//             infinite: true,
//             responsive: [
//                 {
//                     breakpoint: 1521,
//                         settings: {
//                             slidesToShow: 3,
//                                 slidesToScroll: 3
//                         }
//                     },
            
//                 {
//                     breakpoint: 768,
//                         settings: {
//                             slidesToShow: 1,
//                                 slidesToScroll: 1
//                         }

//                 }
//             ]
// });