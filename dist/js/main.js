window.onload = function () {

    $('.bonus__item-title').click(function(){
        $(this).toggleClass('active');
        $(this).siblings().toggleClass('show');
        // $(this).siblings().slideToggle();
        $(window).trigger('resize').trigger('scroll');
        // setTimeout(function(){
        //     $(window).trigger('resize').trigger('scroll');
        // }, 350);
    });

    $('.mobile__menu').click(function(e){
        e.preventDefault();
        $('.nav').slideToggle();
    });


    $('a[href^="#"].navig').click(function () {
        if($(window).innerWidth() <= 992) {
           $('.nav').slideToggle(); 
        }
        elementClick = $(this).attr("href");
        destination = $(elementClick).offset().top-150;
        $('html').animate( { scrollTop: destination }, 500, 'swing' );
        $('body').animate( { scrollTop: destination }, 500, 'swing' );
        return false;
    });
      
};